import time
import serial
import re

class VATPM5:


    nodeID = ""
    defaultBaud = 19200
    defaultTimeout = .1
    defaultPortID = '/dev/tty_Pendulum_Valve'
    defaultParity = serial.PARITY_EVEN
    defaultBytesize = 7
    invaliddefine = -1214
    valvestatus = dict()
    
    def ValveMode(self):
        # Get mode
        try:
            response = self.Send('M:\r\n'.encode())
            responsestringarray = response.decode().split()
            
            if len(responsestringarray)==2:
                
                self.valvestatus['controlstatus'] = responsestringarray[1]
                
            
            else:
                self.valvestatus['controlstatus'] = self.invaliddefine  
            
            return self.valvestatus['controlstatus']

        except:
            self.valvestatus['controlstatus'] = self.invaliddefine  
        
        return self.valvestatus['controlstatus']

    

    def ValvePosition(self):
        try:
            response = self.Send('A:\r\n'.encode())

            # decode into a string
            responsestringarray = response.decode()
            # strip everything that isn't a number, negative, decimal, or engineering notation, then cast to int)

            decodedvalue = int(re.sub("[^-E0-9. ]","",responsestringarray))

            # PM5 controller limits position readback to 0 to 1000    
            if decodedvalue > 1000 or decodedvalue <0:
                self.valvestatus['valveposition'] = self.invaliddefine
            else:
                self.valvestatus['valveposition'] = decodedvalue/10

        except:
            self.valvestatus['valveposition'] = self.invaliddefine
            

        return self.valvestatus['valveposition']
        

    def GaugePressure(self):
        try:
            response = self.Send('P:\r\n'.encode())

            # decode into a string
            responsestringarray = response.decode()
            # strip everything that isn't a number, negative, decimal, or engineering notation, then cast to int)

            decodedvalue = int(re.sub("[^-E0-9. ]","",responsestringarray))

            # PM5 controller limits position readback to 0 to 1000    
            if decodedvalue > 1000:
                print("value greater than max: " + responsestringarray)
                self.valvestatus['pressurevalue'] = 1000
            elif decodedvalue <0:
                print("value less than 0: " + responsestringarray)
                self.valvestatus['pressurevalue'] = 0
            
            else:
                self.valvestatus['pressurevalue'] = decodedvalue
                
        except:
            self.valvestatus['pressurevalue'] = self.invaliddefine
            print(responsestringarray)
            

        return self.valvestatus['pressurevalue']

    def CloseValve(self):
        
        return self.Send('C:\r\n'.encode())

    def OpenValve(self):
        
        return self.Send('O:\r\n'.encode())
        
        
    def PressureCommand(self,value):
        
        value = int(self.BoundInput(value,0,1000)) # all pressure commands are in gauge units; 1000 = gauge full scale.
        print(str(value) + ": " + ('S:'+str((value)).zfill(6)+'\r\n'))
        response = self.Send(('S:'+str((value)).zfill(6)+'\r\n').encode())
        print(response)
        return response
    
    def PositionCommand(self,value):
        
        value = int(self.BoundInput(value,0,1000))

        response = self.Send(('R:'+str((value)).zfill(6)+'\r\n').encode())

        return response

    def RemoteMode(self):

        # puts valve in remote mode
        return self.Send('U:01\r\n'.encode())

    def Send(self,command):
        
        self.VATPM5.write(command)
        
        return self.VATPM5.readline()
	 
    def StartSerial(self,baud=defaultBaud,serialPort=defaultPortID,timeout=defaultTimeout,parity=defaultParity,bytesize=defaultBytesize):
        
        self.VATPM5 = serial.Serial()
        self.VATPM5.bytesize = bytesize
        self.VATPM5.timeout=timeout
        self.VATPM5.port=serialPort
        self.VATPM5.baudrate=baud
        self.VATPM5.parity=parity
        self.VATPM5.open()      
        
        return self.VATPM5       

    def BoundInput(self,value,lowerbound,upperbound):
        
        if value<lowerbound:
            
            value = lowerbound
        
        elif value>upperbound:
            
            value = upperbound
            
        return value

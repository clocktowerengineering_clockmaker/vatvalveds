#!/usr/bin/env python
# -*- coding:utf-8 -*-


# ############################################################################
#  license :
# ============================================================================
#
#  File :        VATPM5DS.py
#
#  Project :     rareRF
#
# This file is part of Tango device class.
# 
# Tango is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Tango is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Tango.  If not, see <http://www.gnu.org/licenses/>.
# 
#
#  $Author :      mark.amato$
#
#  $Revision :    $
#
#  $Date :        $
#
#  $HeadUrl :     $
# ============================================================================
#            This file is generated by POGO
#     (Program Obviously used to Generate tango Object)
# ############################################################################

__all__ = ["VATPM5DS", "VATPM5DSClass", "main"]

__docformat__ = 'restructuredtext'

from calendar import c
import PyTango
import sys
# Add additional import
#----- PROTECTED REGION ID(VATPM5DS.additionnal_import) ENABLED START -----#
import VATPM5
from datetime import datetime
from datetime import timedelta
import threading
import traceback
from scipy import interpolate
import time

#----- PROTECTED REGION END -----#	//	VATPM5DS.additionnal_import

# Device States Description
# CLOSE : Valve is closed.
# FAULT : Valve is in fault.
# MOVING : Valve is moving (in pressure control mode).
# OPEN : Valve is full open.
# RUNNING : Valve is in position control mode.


class VATPM5DS (PyTango.LatestDeviceImpl):
    """This device server is designed to allow RS232 control of any VAT Valve product via the service (RS232) port.
    
    This has been tested with a 612 butterfly valve with two different firmwares, but should also work with pendulum valves as well based on experience with VAT and overall protocol similarity.
    
    The communications protocol is not exposed by VAT so we had to sniff it backwards using tdevmon and the VAT supplied CPA3.0 software. Thanks to the tibbo software devs for their tools."""
    
    # -------- Add you global variables here --------------------------
    #----- PROTECTED REGION ID(VATPM5DS.global_variables) ENABLED START -----#
    lastStatus = datetime.now()
    queueData = 0
    queueClose = False
    queuePressure = False
    queuePosition = False
    queueOpen = False
    queueSend = False
    
    #----- PROTECTED REGION END -----#	//	VATPM5DS.global_variables

    def __init__(self, cl, name):
        PyTango.LatestDeviceImpl.__init__(self,cl,name)
        self.debug_stream("In __init__()")
        VATPM5DS.init_device(self)
        #----- PROTECTED REGION ID(VATPM5DS.__init__) ENABLED START -----#
        
        #----- PROTECTED REGION END -----#	//	VATPM5DS.__init__
        
    def delete_device(self):
        self.debug_stream("In delete_device()")
        #----- PROTECTED REGION ID(VATPM5DS.delete_device) ENABLED START -----#
        
        #----- PROTECTED REGION END -----#	//	VATPM5DS.delete_device

    def init_device(self):
        self.debug_stream("In init_device()")
        self.get_device_properties(self.get_device_class())
        self.attr_controlsStatus_read = ""
        self.attr_valvePosition_read = 0.0
        self.attr_gaugePressure_read = 0.0
        self.attr_positionModeActive_read = False
        self.attr_pressureModeActive_read = False
        #----- PROTECTED REGION ID(VATPM5DS.init_device) ENABLED START -----#
        self.set_state(PyTango.DevState.INIT)
        
        self.ioThread = threading.Thread(target = self.IOMethod)
        self.ioThread.setDaemon(True)
        self.ioThread.start() 
        #----- PROTECTED REGION END -----#	//	VATPM5DS.init_device

    def always_executed_hook(self):
        self.debug_stream("In always_excuted_hook()")
        #----- PROTECTED REGION ID(VATPM5DS.always_executed_hook) ENABLED START -----#
        
        #----- PROTECTED REGION END -----#	//	VATPM5DS.always_executed_hook

    # -------------------------------------------------------------------------
    #    VATPM5DS read/write attribute methods
    # -------------------------------------------------------------------------
    
    def read_controlsStatus(self, attr):
        self.debug_stream("In read_controlsStatus()")
        #----- PROTECTED REGION ID(VATPM5DS.controlsStatus_read) ENABLED START -----#

        attr.set_value(self.attr_controlsStatus_read)
        
        #----- PROTECTED REGION END -----#	//	VATPM5DS.controlsStatus_read
        
    def read_valvePosition(self, attr):
        self.debug_stream("In read_valvePosition()")
        #----- PROTECTED REGION ID(VATPM5DS.valvePosition_read) ENABLED START -----#
        attr.set_value(self.attr_valvePosition_read)
        
        #----- PROTECTED REGION END -----#	//	VATPM5DS.valvePosition_read
        
    def write_valvePosition(self, attr):
        self.debug_stream("In write_valvePosition()")
        data = attr.get_write_value()
        #----- PROTECTED REGION ID(VATPM5DS.valvePosition_write) ENABLED START -----#
        self.queuePositionData = data
        self.queuePosition = True
        
        #----- PROTECTED REGION END -----#	//	VATPM5DS.valvePosition_write
        
    def read_gaugePressure(self, attr):
        self.debug_stream("In read_gaugePressure()")
        #----- PROTECTED REGION ID(VATPM5DS.gaugePressure_read) ENABLED START -----#
        attr.set_value(self.attr_gaugePressure_read)
        
        #----- PROTECTED REGION END -----#	//	VATPM5DS.gaugePressure_read
        
    def write_gaugePressure(self, attr):
        self.debug_stream("In write_gaugePressure()")
        data = attr.get_write_value()
        #----- PROTECTED REGION ID(VATPM5DS.gaugePressure_write) ENABLED START -----#
        self.queuePressureData = data
        self.queuePressure = True
        #----- PROTECTED REGION END -----#	//	VATPM5DS.gaugePressure_write
        
    def read_positionModeActive(self, attr):
        self.debug_stream("In read_positionModeActive()")
        #----- PROTECTED REGION ID(VATPM5DS.positionModeActive_read) ENABLED START -----#
        attr.set_value(self.attr_positionModeActive_read)
        
        #----- PROTECTED REGION END -----#	//	VATPM5DS.positionModeActive_read
        
    def read_pressureModeActive(self, attr):
        self.debug_stream("In read_pressureModeActive()")
        #----- PROTECTED REGION ID(VATPM5DS.pressureModeActive_read) ENABLED START -----#
        attr.set_value(self.attr_pressureModeActive_read)
        
        #----- PROTECTED REGION END -----#	//	VATPM5DS.pressureModeActive_read
        
    
    
            
    def read_attr_hardware(self, data):
        self.debug_stream("In read_attr_hardware()")
        #----- PROTECTED REGION ID(VATPM5DS.read_attr_hardware) ENABLED START -----#
        
        #----- PROTECTED REGION END -----#	//	VATPM5DS.read_attr_hardware


    # -------------------------------------------------------------------------
    #    VATPM5DS command methods
    # -------------------------------------------------------------------------
    
    def Open(self):
        """ Commands a full open valve.
        """
        self.debug_stream("In Open()")
        #----- PROTECTED REGION ID(VATPM5DS.Open) ENABLED START -----#
        self.queueOpen = True
        #----- PROTECTED REGION END -----#	//	VATPM5DS.Open
        
    def Close(self):
        """ Closes the valve.
        """
        self.debug_stream("In Close()")
        #----- PROTECTED REGION ID(VATPM5DS.Close) ENABLED START -----#
        self.queueClose = True
        #----- PROTECTED REGION END -----#	//	VATPM5DS.Close
        
    def PressureControl(self, argin):
        """ Sets the valve into pressure control mode with a target defined in mTorr.  Max value is 1000, min of 0.
        :param argin: PressureTarget
        :type argin: PyTango.DevFloat
        """
        self.debug_stream("In PressureControl()")
        #----- PROTECTED REGION ID(VATPM5DS.PressureControl) ENABLED START -----#
        #print argin
        self.queuePressureData = argin
        self.queuePressure = True
        #----- PROTECTED REGION END -----#	//	VATPM5DS.PressureControl
        
    def PositionControl(self, argin):
        """ Sets the valve to a specific position.  Max value of 100, min of 0.
        :param argin: valvePosition
        :type argin: PyTango.DevFloat
        """
        self.debug_stream("In PositionControl()")
        #----- PROTECTED REGION ID(VATPM5DS.PositionControl) ENABLED START -----#
        self.queuePositionData = argin
        self.queuePosition = True    
        
        #----- PROTECTED REGION END -----#	//	VATPM5DS.PositionControl
        
    def Send(self, argin):
        """ Allows a string to be sent and read back.
        :param argin: 
        :type argin: PyTango.DevString
        :rtype: PyTango.DevString
        """
        self.debug_stream("In Send()")
        argout = ""
        #----- PROTECTED REGION ID(VATPM5DS.Send) ENABLED START -----#
        self.queueSendData = argin
        self.queueSend = True

        while self.queueSend:
        
            pass
        
        argout = self.queueSendData
        #----- PROTECTED REGION END -----#	//	VATPM5DS.Send
        return argout
        

    #----- PROTECTED REGION ID(VATPM5DS.programmer_methods) ENABLED START -----#
    
    def IOMethod(self):


        while True:
            try:
                xvalues = [-10000,0,1000]
                yvalues = [0,0,self.GaugeDefinitions]

                self.gaugeInterpolate = interpolate.interp1d(xvalues,yvalues)

                gaugeScalar = 1000/self.GaugeDefinitions


                self.VATPM5 = VATPM5.VATPM5()
                self.VATPM5.StartSerial(baud=self.defaultBaud,serialPort=self.defaultPortID)
                self.set_state(PyTango.DevState.ON)
                
        # this is the method that handles all serial comms asynchronously from the tango layer.

                self.VATPM5.RemoteMode()

                # if the sensor setup is defined correctly in the database, send it

                if len(self.SensorSetup)==7:

                    self.VATPM5.Send(('s:'+ str(self.SensorSetup) + '\r\n').encode())

                # ok, now loop forever
                try:

                        type(self.maxSlewRate)

                except:
                    print("maximum slew rate not set in device properties. Using rapid slew.")
                    self.maxSlewRate = .000001
                slewing = False
                lastTime = datetime.now()


                while(True):
                    # set maxSlewRate
                    
                    valvePosition = self.VATPM5.ValvePosition()
                    self.attr_gaugePressure_read = self.gaugeInterpolate(self.VATPM5.GaugePressure())
                    self.attr_controlsStatus_read = self.VATPM5.ValveMode()
                        
                    if self.get_state() != PyTango.DevState.ALARM:

                        if self.attr_controlsStatus_read == "POS":
                            
                            self.attr_pressureModeActive_read = False
                            self.attr_positionModeActive_read = True
                            self.set_state(PyTango.DevState.RUNNING)

                        elif self.attr_controlsStatus_read == "PRESS":

                            self.attr_pressureModeActive_read = True
                            self.attr_positionModeActive_read = False
                            self.set_state(PyTango.DevState.MOVING)

                        if int(valvePosition) == 0:
                            self.attr_valvePosition_read = valvePosition
                            self.set_state(PyTango.DevState.CLOSE)

                        elif int(valvePosition) == 100:
                            
                            self.attr_valvePosition_read = valvePosition
                            self.set_state(PyTango.DevState.OPEN)

                        elif int(valvePosition) == -1214:
                            self.attr_valvePosition_read = -1
                            self.set_state(PyTango.DevState.FAULT)
                            raise Exception

                        else:
                            self.attr_valvePosition_read = valvePosition

                        if self.queuePosition:
                            self.queuePosition = False
                            self.VATPM5.PositionCommand(self.queuePositionData*10)

                        if self.queuePressure:
                            
                            '''
                            Slew rate limiting behavior per SOW15-349.
                                If the mode is currently position mode, immediately jump to the new pressure and end queuePressure.
                                Else if not presently slewing, set the slewing bit and set the commanded pressure to the current read pressure.
                                Else slewing:
                                    wait until maxSlewRate time has passed.
                                    
                                    If the current pressure is within 1 mTorr of the requested end pressure, send final pressure, set slewing false, and end queuePressure.

                                    elIf the current commanded pressure is less than the end pressure but more than 1 mtorr away, 
                                    add 1 to the current pressure and send to the valve

                                    Else the current pressure is more than the end pressure but more than 1 mtorr away, 
                                    substract 1 from the current commanded pressure and send to the valve

                            '''
                            
                            if self.attr_controlsStatus_read == "POS":

                                print("Initial set pressure")
                                self.VATPM5.PressureCommand(self.queuePressureData*gaugeScalar)
                                currentCommandedPressure = self.queuePressureData
                                lastTime = datetime.now()
                            
                            elif not slewing:

                                print("into slewing")
                                slewing = True
                                currentCommandedPressure = self.attr_gaugePressure_read
                                self.VATPM5.PressureCommand(currentCommandedPressure*gaugeScalar)
                                lastTime = datetime.now()
                            
                            else:
                                
                                timeelapsed = datetime.now()-lastTime
                               

                                if timeelapsed.seconds > self.maxSlewRate :
                                    
                                    endPressure = self.queuePressureData

                                    if abs(currentCommandedPressure - endPressure)<1:
                                        
                                        print("last pressure, stopping slew")
                                        self.VATPM5.PressureCommand(self.queuePressureData*gaugeScalar)
                                        lastTime = datetime.now()
                                        slewing = False
                                        self.queuePressure = False
                                    
                                    elif currentCommandedPressure < endPressure:
                                        
                                        print(str(currentCommandedPressure) + " < " + str(endPressure) + ", slewing")

                                        currentCommandedPressure = currentCommandedPressure + 1
                                        self.VATPM5.PressureCommand(currentCommandedPressure*gaugeScalar)
                                        lastTime = datetime.now()

                                    else:

                                        print(str(currentCommandedPressure) + " > " + str(endPressure) + ", slewing")

                                        currentCommandedPressure = currentCommandedPressure - 1
                                        self.VATPM5.PressureCommand(currentCommandedPressure*gaugeScalar)
                                        lastTime = datetime.now()

                        if self.queueOpen:
                            self.queueOpen = False
                            self.VATPM5.OpenValve()

                        if self.queueClose:
                            self.queueClose = False
                            self.VATPM5.CloseValve()

                        if self.queueSend:
                            print(self.queueSendData)
                            self.queueSendData = self.VATPM5.Send(str(self.queueSendData).encode())
                            print(self.queueSendData)
                            self.queueSend = False
                        
            except Exception as e:
                print('Serial device failure at ' + str(datetime.now()))
                traceback.print_exc()
                self.set_state(PyTango.DevState.FAULT)
                time.sleep(5)
        
    #----- PROTECTED REGION END -----#	//	VATPM5DS.programmer_methods

class VATPM5DSClass(PyTango.DeviceClass):
    # -------- Add you global class variables here --------------------------
    #----- PROTECTED REGION ID(VATPM5DS.global_class_variables) ENABLED START -----#
    
    #----- PROTECTED REGION END -----#	//	VATPM5DS.global_class_variables


    #    Class Properties
    class_property_list = {
        }


    #    Device Properties
    device_property_list = {
        'defaultBaud':
            [PyTango.DevDouble, 
            "Default Baud rate",
            [38400]],
        'defaultPortID':
            [PyTango.DevString, 
            "Serial port ID.",
            ["/dev/tty_ExhaustValve"] ],
        'GaugeDefinitions':
            [PyTango.DevFloat, 
            "Defines the gauge parameters.\nEnter the maximum range of the gauge in mTorr.",
            [] ],
        'SensorSetup':
            [PyTango.DevString, 
            "Defines the sensor string to be sent to the controller at device startup.",
            [] ],
        'maxSlewRate':
            [PyTango.DevFloat, 
            "Limits the slew rate of the system. \nValue given seconds per mtorr.\nIf not set defaults to infintely short (jumps straight to setpoint).",
            [0.000001]],
        }


    #    Command definitions
    cmd_list = {
        'Open':
            [[PyTango.DevVoid, "none"],
            [PyTango.DevVoid, "none"]],
        'Close':
            [[PyTango.DevVoid, "none"],
            [PyTango.DevVoid, "none"]],
        'PressureControl':
            [[PyTango.DevFloat, "PressureTarget"],
            [PyTango.DevVoid, "none"]],
        'PositionControl':
            [[PyTango.DevFloat, "valvePosition"],
            [PyTango.DevVoid, "none"]],
        'Send':
            [[PyTango.DevString, "none"],
            [PyTango.DevString, "none"]],
        }


    #    Attribute definitions
    attr_list = {
        'controlsStatus':
            [[PyTango.DevString,
            PyTango.SCALAR,
            PyTango.READ],
            {
                'label': "controlword",
                'description': "reflects the control word status.",
            } ],
        'valvePosition':
            [[PyTango.DevFloat,
            PyTango.SCALAR,
            PyTango.READ_WRITE],
            {
                'label': "ValveOpenness",
                'unit': "%",
                'format': "%3.2f",
                'max value': "100",
                'min value': "0",
                'description': "Valveposition.  100% = full open, 0% = full close.",
            } ],
        'gaugePressure':
            [[PyTango.DevFloat,
            PyTango.SCALAR,
            PyTango.READ_WRITE],
            {
                'label': "Upstream Gauge Pressure",
                'unit': "torr",
                'format': "%3.3e",
                'max value': "1000",
                'min value': "0",
                'description': "Value of the gauge driving the control loop, reported in mTorr.",
                'period': "50",
            } ],
        'positionModeActive':
            [[PyTango.DevBoolean,
            PyTango.SCALAR,
            PyTango.READ],
            {
                'description': "Position mode Active",
            } ],
        'pressureModeActive':
            [[PyTango.DevBoolean,
            PyTango.SCALAR,
            PyTango.READ],
            {
                'description': "Position mode Active",
            } ],
        }


def main():
    try:
        py = PyTango.Util(sys.argv)
        py.add_class(VATPM5DSClass, VATPM5DS, 'VATPM5DS')
        #----- PROTECTED REGION ID(VATPM5DS.add_classes) ENABLED START -----#
        
        #----- PROTECTED REGION END -----#	//	VATPM5DS.add_classes

        U = PyTango.Util.instance()
        U.server_init()
        U.server_run()

    except PyTango.DevFailed as e:
        print ('-------> Received a DevFailed exception:', e)
    except Exception as e:
        print ('-------> An unforeseen exception occured....', e)

if __name__ == '__main__':
    main()

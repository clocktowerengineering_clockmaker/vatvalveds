import time
import serial

class VATValve:


    nodeID = ""
    defaultBaud = 38400
    defaultTimeout = .1
    defaultPortID = '/dev/tty_ExhaustValve'
    defaultParity = serial.PARITY_EVEN
    defaultBytesize = 7
    invaliddefine = -1214
    valvestatus = dict()
    
    def ValveStatus(self):
        
        return self.Send('x:10\r\n'.encode()).decode()
        
    def CloseValve(self):
        
        return self.Send('C:\r\n'.encode()).decode()

    def OpenValve(self):
        
        return self.Send('O:\r\n'.encode()).decode()
        
    def EnableSerialControl(self):
        
        return self.Send('c:0100\r\n'.encode()).decode()
        
    def EnableAnalogControl(self):
        
        return self.Send('c:0101\r\n'.encode()).decode()
        
    def PressureCommand(self,value):
        
        value = int(self.BoundInput(value,0,1200)/20) # 2 Torr gauge. TODO: don't hardcode this.
        #print value
        self.EnableSerialControl()
        
        return self.Send(('S:'+str((value)*100).zfill(6)+'00\r\n').encode()).decode()
    
    def PositionCommand(self,value):
        
        value = int(self.BoundInput(value,0,100))
        self.EnableSerialControl()
        return self.Send(('R:'+str((value)*1000).zfill(6)+'\r\n').encode()).decode()

    def ParseResponse(self,responseline):

        responseline = str(responseline)
        try:
            if len(responseline)<2:
                
                # something is wrong with serial comms.  Time out. invalid.
                self.valvestatus['commandresponse'] = self.invaliddefine
                self.valvestatus['valveposition'] = self.invaliddefine
                self.valvestatus['pressurevalue'] = self.invaliddefine
                self.valvestatus['controlstatus']= self.invaliddefine
        
            
                    
            elif responseline[0:1] == 'E':
                
                # error.
                self.valvestatus['commandresponse'] = responseline[0:4]
                self.valvestatus['valveposition'] = self.invaliddefine
                self.valvestatus['pressurevalue'] = self.invaliddefine
                self.valvestatus['controlstatus']= self.invaliddefine
            
            else:
                
                self.valvestatus['commandresponse'] = responseline[0:4]
                self.valvestatus['valveposition'] = int(responseline[4:10])/1000
                self.valvestatus['pressurevalue'] = int(responseline[12:18])/5E5 # 2 Torr gauge
                self.valvestatus['controlstatus']= int(responseline[32:33])/1
            
        except:
            
            self.valvestatus['commandresponse'] = self.invaliddefine
            self.valvestatus['valveposition'] = self.invaliddefine
            self.valvestatus['pressurevalue'] = self.invaliddefine
            self.valvestatus['controlstatus']= self.invaliddefine
        
        
        return self.valvestatus

    def Send(self,command):
        
        self.VATValve.write(command)
        
        return self.VATValve.readline()
	 
    def StartSerial(self,baud=defaultBaud,serialPort=defaultPortID,timeout=defaultTimeout,parity=defaultParity,bytesize=defaultBytesize):
        
        self.VATValve = serial.Serial()
        self.VATValve.bytesize = bytesize
        self.VATValve.timeout=timeout
        self.VATValve.port=serialPort
        self.VATValve.baudrate=baud
        self.VATValve.parity=parity
        self.VATValve.open()      
        
        return self.VATValve       

    def BoundInput(self,value,lowerbound,upperbound):
        
        if value<lowerbound:
            
            value = lowerbound
        
        elif value>upperbound:
            
            value = upperbound
            
        return value
